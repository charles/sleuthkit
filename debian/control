Source: sleuthkit
Section: admin
Priority: optional
Maintainer: Debian Security Tools <team+pkg-security@tracker.debian.org>
Build-Depends: debhelper-compat (= 13),
               libafflib-dev (>= 3.6.6),
               libewf-dev (>= 20130416),
               libsqlite3-dev,
               libvhdi-dev,
               libvmdk-dev,
Standards-Version: 4.6.2
Rules-Requires-Root: no
Homepage: http://www.sleuthkit.org/sleuthkit
Vcs-Browser: https://salsa.debian.org/pkg-security-team/sleuthkit
Vcs-Git: https://salsa.debian.org/pkg-security-team/sleuthkit.git

Package: sleuthkit
Architecture: any
Depends: file,
         libdate-manip-perl,
         ${misc:Depends},
         ${perl:Depends},
         ${shlibs:Depends}
Suggests: autopsy, mac-robber
Conflicts: tct
Description: tools for forensics analysis on volume and filesystem data
 The Sleuth Kit, also known as TSK, is a collection of UNIX-based command
 line file and volume system forensic analysis tools. The filesystem tools
 allow you to examine filesystems of a suspect computer in a non-intrusive
 fashion. Because the tools do not rely on the operating system to process the
 filesystems, deleted and hidden content is shown.
 .
 The volume system (media management) tools allow you to examine the layout of
 disks and other media. You can also recover deleted files, get information
 stored in slack spaces, examine filesystems journal, see partitions layout on
 disks or images etc. But is very important clarify that the TSK acts over the
 current filesystem only.
 .
 The Sleuth Kit supports DOS partitions, BSD partitions (disk labels), Mac
 partitions, Sun slices (Volume Table of Contents), and GPT disks. With these
 tools, you can identify where partitions are located and extract them so that
 they can be analyzed with filesystem analysis tools.
 .
 Currently, TSK supports several filesystems, as NTFS, FAT, exFAT, HFS+, Ext3,
 Ext4, UFS and YAFFS2.
 .
 This package contains the set of command line tools in The Sleuth Kit.

Package: libtsk19
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Conflicts: libtsk10v5
Replaces: libtsk10v5
Description: library for forensics analysis on volume and filesystem data
 The Sleuth Kit, also known as TSK, is a collection of UNIX-based command
 line file and volume system forensic analysis tools. The filesystem tools
 allow you to examine filesystems of a suspect computer in a non-intrusive
 fashion. Because the tools do not rely on the operating system to process the
 filesystems, deleted and hidden content is shown.
 .
 The volume system (media management) tools allow you to examine the layout of
 disks and other media. You can also recover deleted files, get information
 stored in slack spaces, examine filesystems journal, see partitions layout on
 disks or images etc. But is very important clarify that the TSK acts over the
 current filesystem only.
 .
 The Sleuth Kit supports DOS partitions, BSD partitions (disk labels), Mac
 partitions, Sun slices (Volume Table of Contents), and GPT disks. With these
 tools, you can identify where partitions are located and extract them so that
 they can be analyzed with filesystem analysis tools.
 .
 Currently, TSK supports several filesystems, as NTFS, FAT, exFAT, HFS+, Ext3,
 Ext4, UFS and YAFFS2.
 .
 This package contains the library which can be used to implement all of the
 functionality of the command line tools into an application that needs to
 analyze data from a disk image.

Package: libtsk-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libtsk19 (= ${binary:Version}), zlib1g-dev, ${misc:Depends}
Description: library for forensics analysis (development files)
 The Sleuth Kit, also known as TSK, is a collection of UNIX-based command
 line file and volume system forensic analysis tools. The filesystem tools
 allow you to examine filesystems of a suspect computer in a non-intrusive
 fashion. Because the tools do not rely on the operating system to process the
 filesystems, deleted and hidden content is shown.
 .
 The volume system (media management) tools allow you to examine the layout of
 disks and other media. You can also recover deleted files, get information
 stored in slack spaces, examine filesystems journal, see partitions layout on
 disks or images etc. But is very important clarify that the TSK acts over the
 current filesystem only.
 .
 The Sleuth Kit supports DOS partitions, BSD partitions (disk labels), Mac
 partitions, Sun slices (Volume Table of Contents), and GPT disks. With these
 tools, you can identify where partitions are located and extract them so that
 they can be analyzed with filesystem analysis tools.
 .
 Currently, TSK supports several filesystems, as NTFS, FAT, exFAT, HFS+, Ext3,
 Ext4, UFS and YAFFS2.
 .
 This package contains header files and static version of the library.
